#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <DHT_U.h>
#include "U8glib.h"


#define DHTPIN     2         // Pin connected to the DHT sensor.

// Uncomment the type of sensor in use:
//#define DHTTYPE    DHT11     // DHT 11
#define DHTTYPE    DHT22     // DHT 22 (AM2302)
//#define DHTTYPE    DHT21     // DHT 21 (AM2301)

// See guide for details on sensor wiring and usage:
//   https://learn.adafruit.com/dht/overview



//joystick
#define RIGHT 1
#define UP 2
#define LEFT 3
#define DOWN 4
#define PUSH 5
#define MID 0




int red=9;
int green=10;
int blue=3;



DHT_Unified dht(DHTPIN, DHTTYPE);

uint32_t delayMS;
U8GLIB_SSD1306_128X64 myOled(U8G_I2C_OPT_NONE);
String oledDisplay="";      //the display string recieved from the mobile device

int humidity=0;             //humidity read from DHT11
int temperature=0;          //temperature read from DHT11

int knob=0;                 //knob(potentiometer) value read from analog pin

int buzz=11;
/*
void draw() {
// graphic commands to redraw the complete screen should be placed here
myOled.setFont(u8g_font_unifont);
//u8g.setFont(u8g_font_osb21);
myOled.drawStr( 10, 10, "Hello World!");
}
*/



void draw (void)
{
	myOled.setFont(u8g_font_unifont);
	digitalWrite(buzz,10);
	myOled.setPrintPos(10,16);      //set the print position
	myOled.print("H:");
	myOled.print(humidity);         //show the humidity on oled
	myOled.print("%");

	myOled.setPrintPos(10,32);
	myOled.print("T:");             //show the temperature on oled
	myOled.print(temperature);
	myOled.print("C");
	/*
	myOled.setPrintPos(88,16);
	myOled.print("R:");             //show RGBLED red value
	myOled.print(ledRed);
	myOled.setPrintPos(88,32);
	myOled.print("G:");             //show RGBLED green value
	myOled.print(ledGreen);
	myOled.setPrintPos(88,48);
	myOled.print("B:");             //show RGBLED blue value
	myOled.print(ledBlue);
	*/

	myOled.setPrintPos(10,48);
	myOled.print("Knob:");
	myOled.print(knob);             //show knob(potentiometer) value read from analog pin

	myOled.setPrintPos(10,60);

		myOled.print("Joystick:");  //if string is null, show the state of joystick

		/*
		switch (joyStick){
		case MID:
		myOled.print("Normal");
		break;
		case RIGHT:
		myOled.print("Right");
		break;
		case UP:
		myOled.print("Up");
		break;
		case LEFT:
		myOled.print("Left");
		break;
		case DOWN:
		myOled.print("Down");
		break;
		case PUSH:
		myOled.print("Push");
		break;
		default:
		break;


	}
	*/
	digitalWrite(buzz,0);
}



/*8*/
void setup() {
	// put your setup code here, to run once:
	pinMode(buzz,OUTPUT);
}

void loop() {
	// put your main code here, to run repeatedly:
	// picture loop
	myOled.firstPage();
	do {
		draw();

	} while( myOled.nextPage() );

	// rebuild the picture after some delay
	delay(250);
}
